#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	const float pi = 3.14159265;
	char text[] = "Some text here";
	int number = 1246754;
	double numerical_value = 2674.3236434664311;

	// Uzupelnij kod
	//printf("%.3f\n", pi);
	//printf("%c%c%c\n", text[0], text[2],text[7]);
	//printf("%4i\n", number);
	
	printf("%.8f\n", numerical_value);
	
	system("pause");

	return EXIT_SUCCESS;
}
