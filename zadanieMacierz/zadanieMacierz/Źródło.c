#include <stdlib.h>
#include <stdio.h>

void zero(int tab[][5], int ncolumn, int x)
{
	int i = x - 2;

	for (int j = 0; j < ncolumn; j += x)
	{
		tab[i][j] = 0;
	}
}

void showtab(int tab[][5], int ncolumn, int nrows)
{
	for (size_t i = 0; i != ncolumn; i++)
	{
		printf("\t\t");
		for (size_t j = 0; j != nrows; j++)
		{
			printf("%i ", tab[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
int main()
{

	int tab[][5] =
	{
		{ 2, 7, 14, 20, 11 },
		{ 3, 9, 4, 12, 15 },
		{ 10, 22, 13, 7, 8 },
		{ 55, 72, 43, 91, 1 },
		{ 145, 32, 56, 5, 17 } };

	int nrows = sizeof(tab) / sizeof(tab[0]);
	int ncolumn = sizeof(tab[0]) / sizeof(tab[0][0]);

	showtab(tab, ncolumn, nrows);
	for (int i = 2; i < 6; i++)
	{
		zero(tab, ncolumn, i);
	}

	// for (int i = 0; i<ncolumn; i += 2)
	// {
	//     tab[0][i] = 0;
	// }
	// for (int i = 0; i < ncolumn; i += 3)
	// {
	//     tab[1][i] = 0;
	// }
	// for (int i = 0; i < ncolumn; i += 4)
	// {
	//     tab[2][i] = 0;
	// }
	// for (int i = 0; i < ncolumn; i += 5)
	// {
	//     tab[3][i] = 0;
	// }
	// for (int i = 1; i < ncolumn; i ++)
	// {
	//     if(i%6==0)
	//     {
	//         tab[4][i] = 0;
	//     }
	// }
	showtab(tab, ncolumn, nrows);

	/*    printf("%i\n", tab[0][0]);
	printf("%i\n", tab[0][1]);
	printf("%i\n", tab[0][2]);
	printf("%i\n", tab[0][3]);
	printf("%i\n", tab[0][4]);
	printf("%i\n", tab[0][5]);
	*/

	system("pause");
	return 0;
}