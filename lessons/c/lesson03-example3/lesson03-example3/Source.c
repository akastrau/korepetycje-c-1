#include <stdio.h>
#include <stdlib.h>

void tryToStealSomeBottles(int howMany, int bottles) 
{
	// Zbysio probuje ukrasc 3 cytryny
	bottles -= howMany; // Ukradnij troche buteleczek
}

void stealSomeBottles(int howMany, int* bottles)
{
	// Chytra baba z Radomia wkracza do akcji!
	*bottles -= howMany;
}

int main()
{
	int bottles = 7;

	tryToStealSomeBottles(3, bottles);
	printf("Liczba butelek: %i\n", bottles);

	stealSomeBottles(3, &bottles);
	printf("Liczba butelek: %i\n", bottles);

	system("pause");
	return EXIT_SUCCESS;
}