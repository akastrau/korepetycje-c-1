#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main()
{
	bool doIt = false;
	int counter = 0;

	do
	{
		printf("Do it? ");
		doIt == true ? printf("Yes!\n") : printf("No!\n");
		counter++;
	} while (doIt);
	printf("Counter: %i\n", counter);

	while (doIt == 1) // doIt == true
	{
		printf("Incrementing counter in while loop!\n");
		counter++;
	}
	printf("Counter: %i\n", counter);
	doIt = 1;
	printf("Do it? ");
	doIt == true ? printf("Yes!\n") : printf("No!\n");
	while (doIt == true && counter < 3)
	{
		printf("Incrementing counter in while loop (another loop)!\n");
		counter += 1; // counter++; lub counter = counter + 1;
	}

	system("pause");
	return 0;
}