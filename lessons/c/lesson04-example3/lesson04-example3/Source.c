#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

int main()
{
	int *tab = NULL;
	int howMany = 0;

	printf("Enter the size of your tab: ");
	scanf_s("%i", &howMany);

	tab = (int*)_malloca(howMany * sizeof(int));

	for (int i = 0; i < howMany; i++)
	{
		tab[i] = i;
	}
	printf("Displaying tab...\n");

	for (int i = 0; i < howMany; i++)
	{
		printf("%i ", tab[i]);
	}
	printf("\n");
	_freea(tab);

	system("pause");
	return 0;
}