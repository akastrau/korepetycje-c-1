#include <stdio.h>
#include <stdlib.h>

int sum(int n)
{
	if (n == 0)
		return 0;
	return  n + sum(n - 1);
}

int main()
{
	int n = 4;
	int totalSum = sum(n);
	printf("Sum: %i\n", totalSum);

	system("pause");
	return EXIT_SUCCESS;
}