#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	char answer[4] = { 0 };
	while (1) {
		printf("Do you know Elizabeth Turner? [yes, no, idk]: ");
		scanf_s("%s", answer, 4);

		if (strcmp(answer, "yes") == 0)
		{
			printf("Great!\n");
			break;
		}
		else if (strcmp(answer, "no") == 0) {
			printf("Elizabeth is an actress!\n");
			break;
		}
		else if (strcmp(answer, "idk") == 0) {
			printf("Really!? I'm not sure if you are just stupid or you're trolling!\n");
			break;
		}
		else {
			printf("Do you know Elizabeth Turner? [yes, no, idk]: ");
			scanf_s("%s", answer, 4);
		}
	}
	
	system("pause");
	return 0;
}