#include <stdio.h>
#include <stdlib.h>

void sayMyName(const char* name)
{
	printf("%s!\n", name);
}

int main()
{
	char* myName = "Danka";

	sayMyName(myName);
	sayMyName("Tadzio");

	system("pause");
	return EXIT_SUCCESS;
}