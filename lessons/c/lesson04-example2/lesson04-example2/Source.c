#include <stdio.h>
#include <stdlib.h>

int main()
{
	int howMany = 0; // Ile intow w tablicy vector
	int *vector = NULL; // Nasza tablica (niezaalokowana pamiec)
	printf("Enter the size of your vector: ");
	scanf_s("%i", &howMany);
	printf("Allocating memory on the heap... ");

	// Funkcja calloc zwraca adres pierwszego elementu i zeruje pamieca.a
	// W przypadku niepowodzenia zwraca NULL
	if ((vector = (int*)calloc(howMany, sizeof(int))) == NULL)
	{
		printf("error!\nOut of memory!\n");
		return 1;
	}
	printf("OK!\n");

		// Zapelnianie tablicy intami
		for (int i = 0; i < howMany; i++)
		{
			vector[i] = i;
		}

	// Wyswietlanie
	printf("Displaying vector...\n");
	for (int i = 0; i < howMany; i++)
	{
		printf("%i ", vector[i]);
	}
	printf("\n");

	// BARDZO WAZNE!!!!
	// Zwalnianie pamieci
	// W przypadku niezwolnienia - wyciek pamieci (memory leak)
	free(vector);

	system("pause");
	return 0;
}