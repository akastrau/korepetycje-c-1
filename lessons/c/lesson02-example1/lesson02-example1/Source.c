#include <stdio.h>
#include <stdlib.h>

int main()
{
	char name[35];
	char myName[] = "Alice"; // char myName[] = {'A', 'l', 'i', 'c', 'e'}; lub char* myName = "Alice";
	int age = 0;
	printf("Hi, I'm %s! What's your name?: ", myName);
	scanf("%s", name);

	printf("Hi %s! How old are you? ", name);
	scanf("%i", &age);

	if (age < 18)
	{
		printf("This content is not available for you! Sorry, dude!\n");
		printf("Returning -1!\n");
		system("pause");
		return -1;
	}
	else 
	{
		printf("Ok, You're in!\n");
	}
	
	system("pause");
	return 0;
}